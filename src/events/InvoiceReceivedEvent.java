package events;

import enitities.Invoice;

public class InvoiceReceivedEvent {

    private Invoice invoice;

    public InvoiceReceivedEvent(Invoice invoice) {
        this.invoice = invoice;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }
}
