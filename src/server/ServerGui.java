package server;

import enitities.Invoice;
import enitities.Product;
import events.InvoiceReceivedEvent;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;


public class ServerGui extends JFrame {
    private JLabel separator;
    private JButton saveButton;
    private JTextArea productCountArea;
    private JComboBox productBoxList;
    private JTextArea productNameArea;
    private JTextArea productPriceArea;
    private boolean isEditButtonPressed = false;
    private boolean isAddNewButtonPressed = false;
    private ServerNetwork network;
    private JPanel panel;

    private List<Product> products;
    private Product selectedProduct;
    private JLabel ordersTitle;

    public ServerGui() {
        System.out.println("Gui started!");
        ProductDbHandler productDbHandler = new ProductDbHandler();
        productDbHandler.initializeProductsAndPopulateDB();
        products = productDbHandler.loadProductsFromDB();
        List<Product> products1 = productDbHandler.loadProductsFromDB();
        EventBus.getDefault().register(this);

        setSize(1200, 700);
        setTitle("Server");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);

        createElements();
        this.setVisible(true);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.out.println("Closing win and saving perfomed!");
                productDbHandler.saveProductsToFile(products);
                e.getWindow().dispose();
            }
        });
        network = new ServerNetwork(products);
    }

    private void createElements() {


        //Mockup
//        String[] str = new String[]{"k","s","m"};
//
//        List<Product> list = new ArrayList<>();
//        list.add(new Product("a",1,1));
//        list.add(new Product("b",2,2));
//        list.add(new Product("c",3,3));

        ordersTitle = new JLabel();
        ordersTitle.setText("Narudzbine");
        ordersTitle.setSize(200, 25);
        ordersTitle.setLocation(650, 50);
        this.add(ordersTitle);
        //==================================================
        panel = new JPanel();

        panel.setSize(850,550);
        panel.setBackground(Color.GRAY);
        panel.setLocation(500, 100);
        this.add(panel);


        //==================================================
        JLabel productLabel = new JLabel();
        productLabel.setText("Prozivod:");
        productLabel.setSize(100, 50);
        productLabel.setLocation(15, 40);
        this.add(productLabel);

        productBoxList = new JComboBox(products.toArray());
        productBoxList.setSelectedIndex(0);
        productBoxList.setSize(200, 25);
        productBoxList.setLocation(100, 50);
        this.add(productBoxList);

        JButton editProductButton = new JButton();
        editProductButton.setText("Izmeni prozivod");
        editProductButton.setSize(150, 25);
        editProductButton.setLocation(325, 50);
        editProductButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editProduct();
                separator.setText("Izmeni proizvod");
                saveButton.setText("Sacuvaj izmene");
                isEditButtonPressed = true;
                isAddNewButtonPressed = false;
            }
        });
        this.add(editProductButton);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        JButton addProductButton = new JButton();
        addProductButton.setText("Dodaj novi prozivod");
        addProductButton.setSize(150, 25);
        addProductButton.setLocation(325, 100);
        addProductButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveButton.setText("Sacuvaj novi prozivod");
                separator.setText("Dodaj novi prozivod");
                addNewProduct();

            }
        });
        this.add(addProductButton);

        JButton deleteProductButton = new JButton();
        deleteProductButton.setText("Obrisi prozivod");
        deleteProductButton.setSize(150, 25);
        deleteProductButton.setLocation(325, 150);
        deleteProductButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                deleteProduct();
                separator.setText("Proizvod obrisan");
                isEditButtonPressed = false;
                isAddNewButtonPressed = false;
            }
        });

        this.add(deleteProductButton);

        int offset = 50;


        separator = new JLabel();
        separator.setText("-------------  Prozivod  -------------");
        separator.setSize(250, 50);
        separator.setLocation(150, 200);
        this.add(separator);


        JLabel inputProductNameLabel = new JLabel();
        inputProductNameLabel.setText("Ime proizvoda:");
        inputProductNameLabel.setSize(130, 50);
        inputProductNameLabel.setLocation(15, 185 + offset);
        this.add(inputProductNameLabel);


        productNameArea = new JTextArea();
        productNameArea.setSize(200, 25);
        productNameArea.setLocation(125, 200 + offset);
        this.add(productNameArea);


        JLabel inputProductPriceLabel = new JLabel();
        inputProductPriceLabel.setText("Cijena:");
        inputProductPriceLabel.setSize(130, 50);
        inputProductPriceLabel.setLocation(15, 235 + offset);
        this.add(inputProductPriceLabel);


        productPriceArea = new JTextArea();
        productPriceArea.setSize(200, 25);
        productPriceArea.setLocation(125, 250 + offset);
        this.add(productPriceArea);


        JLabel inputProductCountLabel = new JLabel();
        inputProductCountLabel.setText("Kolicina:");
        inputProductCountLabel.setSize(130, 50);
        inputProductCountLabel.setLocation(15, 285 + offset);
        this.add(inputProductCountLabel);


        productCountArea = new JTextArea();
        productCountArea.setSize(200, 25);
        productCountArea.setLocation(125, 300 + offset);
        this.add(productCountArea);


        saveButton = new JButton();
        saveButton.setText("Sacuvaj");
        saveButton.setSize(300, 25);
        saveButton.setLocation(100, 400);
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                save();
            }
        });
        this.add(saveButton);

    }

    private void deleteProduct() {

        productNameArea.setText(products.get(productBoxList.getSelectedIndex()).getDescription());
        productPriceArea.setText(products.get(productBoxList.getSelectedIndex()).getPrice() + "");
        productCountArea.setText(products.get(productBoxList.getSelectedIndex()).getProductCount() + "");
        selectedProduct = products.get(productBoxList.getSelectedIndex());
        productBoxList.removeItemAt(productBoxList.getSelectedIndex());
        products.remove(selectedProduct);

    }

    private void editProduct() {

        productNameArea.setText(products.get(productBoxList.getSelectedIndex()).getDescription());
        productPriceArea.setText(products.get(productBoxList.getSelectedIndex()).getPrice() + "");
        productCountArea.setText(products.get(productBoxList.getSelectedIndex()).getProductCount() + "");
        selectedProduct = products.get(productBoxList.getSelectedIndex());
    }

    private void save() {
        if (isEditButtonPressed) {
            separator.setText("Proizvod izmenjen");
            selectedProduct.setDescription(productNameArea.getText());
            selectedProduct.setPrice(Double.parseDouble(productPriceArea.getText()));
            selectedProduct.setProductCount(Integer.parseInt(productCountArea.getText()));
        } else if (isAddNewButtonPressed) {
            separator.setText("Proizvod sacuvan");
            Product product = new Product(productNameArea.getText(), Double.parseDouble(productPriceArea.getText()), Integer.parseInt(productCountArea.getText()));
            products.add(product);
            productBoxList.addItem(product);
            addNewProduct();
            saveButton.setText("Sacuvaj");
        }
    }

    private void addNewProduct() {
        productNameArea.setText("");
        productPriceArea.setText("");
        productCountArea.setText("");
        isAddNewButtonPressed = true;
        isEditButtonPressed = false;

    }

    @Subscribe
    public void onEvent(InvoiceReceivedEvent event) {
        System.out.println("EVENT RECEIVED :D");
        Invoice invoice = event.getInvoice();
        JLabel ordersContent = new JLabel();
        ordersContent.setSize(500, 200);
        panel.add(ordersContent);
        ordersContent.setText( "<html>" + invoice.toString() + "<br></html>");
        panel.add(ordersContent);
    }

}
