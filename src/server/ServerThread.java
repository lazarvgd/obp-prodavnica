package server;

import enitities.Invoice;
import enitities.Product;
import events.InvoiceReceivedEvent;
import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import java.util.logging.Logger;

public class ServerThread extends Thread {
    private static int threadCount = 0;
    private static final Object TAG = "server.ServerThread";
    private final Logger LOGGER = Logger.getLogger(ServerThread.class.getName());
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private Socket socket;
    private List<Product> productList;

    public ServerThread(Socket socket,List<Product> products) {
        this.productList = products;
        System.out.println("STARTING SERVER...");
        System.out.println("Server instance : " + threadCount++);
        this.socket = socket;
        System.out.println("SERVER THREAD!!");
        try {
            in = new ObjectInputStream(socket.getInputStream());
            out = new ObjectOutputStream(socket.getOutputStream());


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        start();
    }


    public void run() {
        Product product=null;
        System.out.println("RUNING SERVER...");
        /*try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        try {
            System.out.println("Sending product list...");
            out.writeObject(productList);
            System.out.println("Product list sent.");
            try {
                Invoice invoice = (Invoice) in.readObject();
                System.out.println(invoice.toString());
                EventBus.getDefault().post(new InvoiceReceivedEvent(invoice));
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void closeConnection() throws IOException {
        in.close();
        out.close();
        socket.close();
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }
}

