package server;

import enitities.Product;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

public class ServerNetwork {

    public static final int PORT_NUMBER = 9000;
    private ServerThread st;
    private List<Product> productList;

    public ServerNetwork(List<Product> productList) {
        this.productList = productList;
        this.runServer();
    }

    private void runServer() {

        try {
            ServerSocket ss = new ServerSocket(PORT_NUMBER);

            while (true) {
                Socket socket = ss.accept();
                System.out.println();

                st = new ServerThread(socket,productList);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateProducts(List<Product> products){
        this.productList = products;
        st.setProductList(productList);
    }

}