package server;

import enitities.Product;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public final class ProductDbHandler {

    //TODO ENTER FILE PATH
    public static final String FILE_PATH = "input.txt";

    private FileOutputStream fileOutputStream = null;
    private ObjectOutputStream objectOutputStream = null;
    private FileInputStream fileInputStream = null;
    private ObjectInputStream objectInputStream = null;

    private void openInputConnectionToDB() {
        try {
            fileInputStream = new FileInputStream(new File(FILE_PATH));
            objectInputStream = new ObjectInputStream(fileInputStream);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void openOutputConnectionToDB() {
        try {
            fileOutputStream = new FileOutputStream(new File(FILE_PATH));
            objectOutputStream = new ObjectOutputStream(fileOutputStream);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public final void saveProductsToFile(List<Product> list) {
        File file = new File(FILE_PATH);
        file.delete();

        try {
            fileOutputStream = new FileOutputStream(new File(FILE_PATH));
            objectOutputStream = new ObjectOutputStream(fileOutputStream);

            objectOutputStream.writeObject(list);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public final List<Product> loadProductsFromDB() {

        List<Product> products = new ArrayList<>();

        products = extractDataFromFile();
        for (Product pr : products) {
            System.out.println(pr.toString());
        }
        closeInputConnectionToDB();
        return products;
    }

    private List<Product> extractDataFromFile() {
        List<Product> products = new ArrayList<>();
        openInputConnectionToDB();
        Product product;
        try {
            products = (List<Product>) objectInputStream.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return products;
    }


    public void initializeProductsAndPopulateDB() {
        openOutputConnectionToDB();
        List<Product> list = new ArrayList<>();
        Product p = new Product("Jogurt", 0.8, 20);
        list.add(p);

        p = new Product("Smoki", 0.5, 20);
        list.add(p);

        p = new Product("Kikiriki", 4, 30);
        list.add(p);

        p = new Product("Masline", 12.3, 40);
        list.add(p);

        p = new Product("Mlijeko", 1.0, 40);
        list.add(p);

        p = new Product("CocaCola", 8.5, 55);
        list.add(p);

        p = new Product("Pivo", 1.8, 54);
        list.add(p);

        p = new Product("Vodka", 2.9, 23);
        list.add(p);

        p = new Product("Kafa", 3.1, 11);
        list.add(p);

        p = new Product("Krompir", 5.0, 100);
        list.add(p);

        p = new Product("Majonez", 1.2, 23);
        list.add(p);
        System.out.println(list.size());

        try {
            objectOutputStream.writeObject(list);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private void closeInputConnectionToDB() {
        try {
            fileInputStream.close();

            objectInputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
