package client;


import enitities.Invoice;
import enitities.Product;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ClientGui extends JFrame {
    private  JLabel shoppingCartLabel;
    private List<Product> productList = new ArrayList<>();
    private List<Product> shoppingCartList = new ArrayList<>();
    private  ClientNetwork clientNetwork;
    private JLabel showQuantity;

    public ClientGui(){

        clientNetwork = new ClientNetwork();
        clientNetwork.startClient();
        productList.addAll(clientNetwork.getProductsAsList());



        setSize(650, 1000);
        setTitle("Client");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);
        createElements();
        setVisible(true);
    }

    private void createElements() {

        JLabel productsLabel = new JLabel("Proizvod: ");
        productsLabel.setSize(100,30);
        productsLabel.setLocation(10,10);
        getContentPane().add(productsLabel);

        JComboBox productsComboBox = new JComboBox(productList.toArray());
        productsComboBox.setSize(130,30);
        productsComboBox.setLocation(100,12);
        getContentPane().add(productsComboBox);

        JLabel priceLabel = new JLabel("Cena: ");
        priceLabel.setSize(100,30);
        priceLabel.setLocation(250,10);
        getContentPane().add(priceLabel);

        JLabel showPriceLabel = new JLabel("0.0");
        showPriceLabel.setSize(50,30);
        showPriceLabel.setLocation(300,10);
        getContentPane().add(showPriceLabel);

        JLabel totalLabel = new JLabel("Ukupno: ");
        totalLabel.setSize(100,30);
        totalLabel.setLocation(350,10);
        getContentPane().add(totalLabel);

        showQuantity = new JLabel("0");
        showQuantity.setSize(50,30);
        showQuantity.setLocation(420,10);
        getContentPane().add(showQuantity);

        JLabel quantityLabel = new JLabel("Kolicina: ");
        quantityLabel.setSize(100,30);
        quantityLabel.setLocation(450,10);
        getContentPane().add(quantityLabel);

        JTextField getQuantityField = new JTextField();
        getQuantityField.setSize(50,30);
        getQuantityField.setLocation(520,10);
        getContentPane().add(getQuantityField);

        JButton addShoppingCart = new JButton("DODAJ U KORPU");
        addShoppingCart.setSize(200,30);
        addShoppingCart.setLocation(370,85);
        getContentPane().add(addShoppingCart);

        shoppingCartLabel = new JLabel(" |===|   KORPA   |===|");
        shoppingCartLabel.setSize(200,30);
        shoppingCartLabel.setLocation(215,160);
        getContentPane().add(shoppingCartLabel);

        JPanel panel = new JPanel();
        panel.setSize(600,350);
        panel.setLocation(15,200);
        panel.setBackground(Color.gray);
        getContentPane().add(panel);

        productsComboBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedIndex = productsComboBox.getSelectedIndex();
                String price = ""+productList.get(selectedIndex).getPrice();
                String quantity = ""+productList.get(selectedIndex).getProductCount();
                showPriceLabel.setText(price);
                showQuantity.setText(quantity);

            }
        });

        addShoppingCart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String quantityString = getQuantityField.getText();
                int quantity = Integer.parseInt(quantityString);
                int selectedIndex = productsComboBox.getSelectedIndex();

                if(productList.get(selectedIndex).getProductCount() < quantity){
                    JOptionPane.showMessageDialog(getContentPane(),
                            "UNETA KOLICINA JE VECA OD RASPOLOZIVE!",
                            "Warning",
                            JOptionPane.WARNING_MESSAGE);
                }else {

                    Product currentProduct = productList.get(selectedIndex);
                    currentProduct.setProductCount(currentProduct.getProductCount()-quantity);

                    shoppingCartList.add(new Product(currentProduct.getDescription(), currentProduct.getPrice(), quantity));
                    showQuantity.setText(currentProduct.getProductCount()+"");
                    shoppingCartLabel.setText("==== Narudzbina poslata... ===");

                    panel.removeAll();
                    panel.revalidate();
                    panel.repaint();

                    for(Product product : shoppingCartList){
                        JLabel label = new JLabel();
                        String productText = "PROIZVOD: "+product.toString()+"  |  CENA: "+product.getPrice()+"  |  KOLICINA: "+getQuantityField.getText();
                        label.setText(productText);
                        panel.add(label);
                        setVisible(true);

                    }

                }

            }
        });


        JButton sendOrder = new JButton();
        sendOrder.setText("Naruci");
        sendOrder.setLocation(370,585);
        sendOrder.setSize(200,30);
        sendOrder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Invoice invoice = new Invoice(new Date(),shoppingCartList);
                clientNetwork.sendInvoice(invoice);

            }
        });
        this.add(sendOrder);


    }


}
