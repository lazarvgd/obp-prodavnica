package client;

import enitities.Invoice;
import enitities.Product;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClientNetwork {
    public static final int PORT_NUMBER = 9000;
    private static int clientCount=0;
    private ObjectInputStream in;
    private Socket socket = null;
    private ObjectOutputStream out;
    private List<Product> productList;

    public ClientNetwork() {
        System.out.println("STARTING CLIENT...");

        try {
            System.out.println("Initializing...");
            InetAddress addr = InetAddress.getByName("127.0.0.1");
            System.out.println("ADDRESS");

            socket = new Socket(addr, PORT_NUMBER);
            System.out.println("SOCKET");

            out = new ObjectOutputStream(socket.getOutputStream());
            System.out.println("OUT");
            in = new ObjectInputStream(socket.getInputStream());
            System.out.println("IN");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void startClient() {
        List<Product> products = null;
        System.out.println("Client started "+clientCount+++ " ...");
        System.out.println("WAITING FOR MSG...");

        getProductListOverNetwork(in);
//            List<Product> productList = new ArrayList<>();
//            productList.add(new Product("PRODUCT",666,66));
//            productList.add(new Product("sarma",56,3231));
//            productList.add(new Product("kamion",46,6));
//            productList.add(new Product("tv",99,999));
//            Invoice inovice = new Invoice(new Date(), productList);
//
//
//
        System.out.println("RECEIVED : " + products);
//            out.writeObject(inovice);

        //            closeAllConnections();


    }

    public void sendInvoice(Invoice invoice){
        try {
            out.writeObject(invoice);
//            closeAllConnections();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getProductListOverNetwork(ObjectInputStream in) {
        try {
            productList = (List<Product>) in.readObject();
            for (Product p : productList) {
                System.out.println(p.toString());
                if(p.getDescription().equals("Jogurt")){
                    System.out.println(p.getProductCount());
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public List<Product> getProductsAsList() {
        return productList;
    }

    private void closeAllConnections() throws IOException {
        in.close();
        out.close();
        socket.close();
    }



}
