package enitities;

import java.io.Serializable;

public class Product implements Serializable {
    private static int counter = 0;
    private final int id;
    private String description;
    private double price;
    private int productCount;


    public Product(String description, double price, int productCount) {
        this.description = description;
        this.price = price;
        this.productCount = productCount;
        this.id = counter++;
    }

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getProductCount() {
        return productCount;
    }

    public void setProductCount(int productCount) {
        this.productCount = productCount;
    }

//
//    @Override
//    public String toString() {
//        return "Product{" +
//                "description='" + description + '\'' +
//                ", price=" + price +
//                ", productCount=" + productCou money for nothingnt +
//                ", id=" + id +
//                '}';
//    }

    @Override
    public String toString() {
        return this.description;
    }
}
