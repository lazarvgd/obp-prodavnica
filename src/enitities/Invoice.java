package enitities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Invoice implements Serializable,Cloneable{
    private static int counter = 0;
    private int id;
    private Date creationDate;
    private List<Product> productList;

    public Invoice(Date creationDate, List<Product> productList) {
        this.creationDate = creationDate;
        this.productList = productList;
        this.id=counter++;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    @Override
    public String toString() {
        return  "Order :" + creationDate +
                "\n, productList=" + productList.toString();
    }
}
